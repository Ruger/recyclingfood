<?php
/* Autocarga (lleva a cabo un include) de cada una de las clases de la carpeta controllers
 para no tener que hacer un include manual de cada una de ellas */

function controllers_autoload($classname){
    include 'controllers/' . $classname . '.php';
}

spl_autoload_register('controllers_autoload');