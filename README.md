# RecyclingFood

**IMPORTANTE**

Para poder abrir la pagina web, hay que realizar un par de configuraciones previas directamente en el código.



En la carpeta config, archivo parameters.php -> Hay que modificar la constante base_url y cambiar el valor de la ruta desde el localhost hasta la raiz del proyecto.

En la raiz del proyecto, archivo .htaccess -> En la linea de ErrorDocument 404, hay que modificar también la ruta (solo hay que cambiar la diferencia entre localhost hasta recycling-food).



Todo lo demás está automatizado para la correcta carga de clases y las distintas vistas de la pagina web. 
Esperemos que esto sea provisional y se pueda encontrar una forma de automatizar la deteccion y modificacion de la ruta desde localhost hasta la raiz del proyecto,
aunque lo recomendable es dejarlo en default y pegar la raiz del proyecto directamente en la carpeta de nuestro localhost


**PROYECTO PARA GESTION DE COMIDA**

Recycling Food es una aplicación web para gestionar los restos de comida que las empresas de alimentación acaban tirando a la basura aunque estén en perfecto estado. 
Se intenta establecer un lugar donde usuarios y empresas puedan reunirse y salir ambos beneficiados: por un lado el cliente se
lleva a casa un producto de calidad, fresco y a muy buen precio, y al mismo tiempo la empresa ve reducido su porcentaje de desviación
al reducir la cantidad de productos que acaban en la basura. Y por encima de todo, el más beneficiado es el medio ambiente.


                            
[](Trello)https://trello.com/b/atWa4472


Clases:
    
1. **Productos**

        (nombre,fecha_cad,precio,descrip,peso/cantidad

        (id.Producto,fecha_compra,precio,descrip,peso/cantidad

        -preparados(pizza,hamburger)
        -fresh_product
        -embasados)

2.  **Cliente**

        (Id,pass,correoE,poblacion(direccion),pagar,descuentos);

3.  **Empresa**
        (Id.empresa,tipo_empresa(mercado,restaurante,comercio),poblacion(direccion,Cp);
        
4.   **Cesta**
        (Id.cesta,Fecha_compra,precio);

        

* [x]  Hacer el Diagrama E_R 

![](./Diagrama%20DIA%20Base%20de%20Datos.png)



* [ ]  Hacer diagrama de clases en UML
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        