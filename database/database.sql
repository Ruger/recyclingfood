CREATE DATABASE food_master;
USE food_master;

CREATE TABLE clientes (
    id          int(255) auto_increment not null,
    nombre      varchar(100) not null,
    apellidos   varchar(255),
    poblacion   varchar(255) not null,
    email       varchar(255) not null,
    password    varchar(255) not null,
    rol         varchar(20),
    imagen      varchar(255),

CONSTRAINT pk_clientes PRIMARY KEY(id),
CONSTRAINT uq_email UNIQUE(email)
)ENGINE=InnoDb;

INSERT INTO clientes VALUES(NULL, 'Ruben', 'Ruger Chambra', 'Gandia', 'ruben_27895@hotmail.com', '$2y$04$IfaZk.768k/Gaj..IMelHeGNxFnOtS45lBRpUEYEK.AwjXvz.LrOa', 'admin', null);
INSERT INTO clientes VALUES(NULL, 'Raul', 'Oriol Sanchez', 'Gandia', 'r.oriolsanchez@gmail.com', '$2y$04$h0aA87UN9Bqfl2HEEZ4ziejlWGuxWGvJBVtAb0B.TpNOK0wfWkIN2', 'admin', null);



CREATE TABLE estados (
    id          int(255) auto_increment not null,
    nombre      varchar(100) not null,

CONSTRAINT pk_estados PRIMARY KEY(id),
CONSTRAINT uq_nombre UNIQUE(nombre)
)ENGINE=InnoDb;

INSERT INTO estados VALUES(NULL, 'En venta');
INSERT INTO estados VALUES(NULL, 'Reservada');
INSERT INTO estados VALUES(NULL, 'Recogida');
INSERT INTO estados VALUES(NULL, 'Agotada');



CREATE TABLE categorias_empresas (
    id          int(255) auto_increment not null,
    nombre      varchar(100) not null,

CONSTRAINT pk_categorias_empresas PRIMARY KEY(id),
CONSTRAINT uq_nombre UNIQUE(nombre)
)ENGINE=InnoDb;

INSERT INTO categorias_empresas VALUES(NULL, 'Supermercado');
INSERT INTO categorias_empresas VALUES(NULL, 'Panaderia');
INSERT INTO categorias_empresas VALUES(NULL, 'Fruteria');
INSERT INTO categorias_empresas VALUES(NULL, 'Restaurante');



CREATE TABLE categorias_productos (
    id          int(255) auto_increment not null,
    nombre      varchar(100) not null,

CONSTRAINT pk_categorias_productos PRIMARY KEY(id),
CONSTRAINT uq_nombre UNIQUE(nombre)
)ENGINE=InnoDb;

INSERT INTO categorias_productos VALUES(NULL, 'Comida rapida');
INSERT INTO categorias_productos VALUES(NULL, 'Verdura');
INSERT INTO categorias_productos VALUES(NULL, 'Fruta');
INSERT INTO categorias_productos VALUES(NULL, 'Bolleria');
INSERT INTO categorias_productos VALUES(NULL, 'Pan');
INSERT INTO categorias_productos VALUES(NULL, 'Carne');
INSERT INTO categorias_productos VALUES(NULL, 'Pescado');
INSERT INTO categorias_productos VALUES(NULL, 'Pasta');



CREATE TABLE empresas (
    id                      int(255) auto_increment not null,
    categoria_empresa_id    int(255) not null,
    nombre                  varchar(100) not null,
    poblacion               varchar(255) not null,
    email                   varchar(255) not null,
    password                varchar(255) not null,
    imagen                  varchar(255),

CONSTRAINT pk_empresas PRIMARY KEY(id),
CONSTRAINT uq_email UNIQUE(email),
CONSTRAINT fk_empresa_categoria FOREIGN KEY(categoria_empresa_id) REFERENCES categorias_empresas(id)
)ENGINE=InnoDb;

INSERT INTO empresas VALUES (NULL, 4, 'McDonalds', 'Gandia', 'mcdonalds@mcdonalds.com', 'explotadores', null);
INSERT INTO empresas VALUES (NULL, 4, 'Burger King', 'Gandia', 'burgerking@burgerking.com', 'explotadores2', null);
INSERT INTO empresas VALUES (NULL, 1, 'Mercadona', 'Gandia', 'mercadona@mercadona.com', 'papelhigienico', null);
INSERT INTO empresas VALUES (NULL, 1, 'Consum', 'Gandia', 'consum@consum.com', 'tarjetaconsum', null);
INSERT INTO empresas VALUES (NULL, 2, 'Panes Paco', 'Gandia', 'panes@panes.com', 'panecitos', null);
INSERT INTO empresas VALUES (NULL, 3, 'Fruteria Tutifruti', 'Gandia', 'frutas@frutas.com', 'malacatones', null);



CREATE TABLE productos (
    id                      int(255) auto_increment not null,
    categoria_producto_id   int(255) not null,
    empresa_id              int(255) not null,
    nombre                  varchar(100) not null,
    descripcion             text,
    precio                  float(100,2) not null,
    stock                   int(255) not null,
    oferta                  varchar(2),
    fecha                   date not null,
    imagen                  varchar(255),

CONSTRAINT pk_productos PRIMARY KEY(id),
CONSTRAINT fk_producto_categoria FOREIGN KEY(categoria_producto_id) REFERENCES categorias_productos(id),
CONSTRAINT fk_producto_empresa FOREIGN KEY(empresa_id) REFERENCES empresas(id)
)ENGINE=InnoDb;



CREATE TABLE cestas (
    id              int(255) auto_increment not null,
    cliente_id      int(255),
    estado_id       int(255) not null,
    poblacion       varchar(255) not null,
    direccion       varchar(255) not null,
    descripcion     text,
    precio          float(200,2) not null,
    fecha           date,
    hora            time,
    imagen          varchar(255),

CONSTRAINT pk_cestas PRIMARY KEY(id),
CONSTRAINT fk_cesta_cliente FOREIGN KEY(cliente_id) REFERENCES clientes(id) on delete SET NULL,
CONSTRAINT fk_cesta_estado FOREIGN KEY(estado_id) REFERENCES estados(id)
)ENGINE=InnoDb;



CREATE TABLE lineas_cesta (
    id              int(255) auto_increment not null,
    cesta_id        int(255) not null,
    producto_id     int(255) not null,
    cantidad        int(255) not null,

CONSTRAINT pk_lineas_cesta PRIMARY KEY(id),
CONSTRAINT fk_linea_cesta_cesta FOREIGN KEY(cesta_id) REFERENCES cestas(id),
CONSTRAINT fk_linea_cesta_producto FOREIGN KEY(producto_id) REFERENCES productos(id)
)ENGINE=InnoDb;