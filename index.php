<?php
/* Controlador frontal que recoge parametros/acciones via URL (GET) y segun a que controlador pertenece
la accion, cargar dicho archivo y llamar al metodo correspondiente */

session_start();
require_once 'autoload.php'; // autocarga de todos los controladores y clases
require_once 'config/db.php'; // carga la base de datos
require_once 'config/parameters.php'; // carga los parametros de configuracion
require_once 'helpers/Utilidades.php'; // carga las utilidades generales
require_once 'views/layout/header.php'; // carga la cabecera
require_once 'views/layout/sidebar.php'; // carga el menu lateral


function show_error(){
    $error = new ErrorController();
    $error->index();
}


if(isset($_GET['controller'])){ // comprueba que llega el parametro controller via GET
    $name_controller = $_GET['controller'] . 'Controller';


}elseif (!isset($_GET['controller']) && !isset($_GET['action'])){
    $name_controller = controller_default;


} else{
    show_error();
    exit();
}


if(class_exists($name_controller)){ // comprueba que exista una clase con el nombre del parametro controller
    $controlador = new $name_controller();

    if(isset($_GET['action']) && method_exists($controlador, $_GET['action'])){ // comprueba que exista un metodo para la accion y el controller indicados
        $action = $_GET['action'];
        $controlador->$action(); // el controlador indicado llama al metodo relacionado con la accion requerida

    }elseif (!isset($_GET['controller']) && !isset($_GET['action'])) {
        $action_default = action_default;
        $controlador->$action_default();

    }else{
        show_error();
    }

}else{
    show_error();
}


require_once 'views/layout/footer.php'; // carga el footer