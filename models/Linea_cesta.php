<?php

class Linea_cesta{
    // variables del modelo Producto
    private $id;
    private $cesta_id;
    private $producto_id;
    private $cantidad;

    private $db;

    public function __construct(){
        $this->db = Database::connect();
    }


    // getters
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCestaId()
    {
        return $this->cesta_id;
    }

    /**
     * @return mixed
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }


    // setters
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $cesta_id
     */
    public function setCestaId($cesta_id)
    {
        $this->cesta_id = $cesta_id;
    }

    /**
     * @param mixed $producto_id
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }




    // metodos que relacionan con la base de datos
    public function save(){
        $sql = "INSERT INTO lineas_cesta VALUES (NULL, {$this->getCestaId()}, {$this->getProductoId()}, {$this->getCantidad()})";
        $save = $this->db->query($sql);

        $result = false;

        if($save) {
            $result = true;
        }

        return $result;
    }



    public function delete(){
        $sql = "DELETE FROM lineas_cesta WHERE id = {$this->id}";
        $delete = $this->db->query($sql);

        $result = false;

        if($delete) {
            $result = true;
        }

        return $result;
    }



    public function edit(){
        $sql = "UPDATE lineas_cesta SET cesta_id = {$this->getCestaId()}, producto_id = {$this->getProductoId()}, cantidad = {$this->getCantidad()}";
        $sql .= " WHERE id = {$this->getId()}";
        $edit = $this->db->query($sql);

        $result = false;

        if($edit) {
            $result = true;
        }

        return $result;
    }



    public function getAll(){
        $lineas = $this->db->query("SELECT * FROM lineas_cesta ORDER BY id DESC");
        return $lineas;
    }


    public function fetchById(){
        $linea = $this->db->query("SELECT * FROM lineas_cesta WHERE id = {$this->getId()}");

        $linea_fetch = $linea->fetch_object();
        $linea_objeto = new Linea_cesta();
        $linea_objeto->setId($linea_fetch->id);
        $linea_objeto->setCestaId($linea_fetch->cesta_id);
        $linea_objeto->setProductoId($linea_fetch->producto_id);
        $linea_objeto->setCantidad($linea_fetch->cantidad);

        return $linea_objeto;
    }


    public function fetchByArguments(){
        $linea = $this->db->query("SELECT * FROM lineas_cesta WHERE cesta_id = {$this->getCestaId()} AND producto_id = {$this->getProductoId()} LIMIT 1");
        return $linea->fetch_object();
    }


    public function fetchByCesta(){
        $lineas = $this->db->query("SELECT * FROM lineas_cesta WHERE cesta_id = {$this->getCestaId()}");
        return $lineas;
    }


    public function modifyStock($stock){
        $sql = "UPDATE productos as p";
        $sql .= " INNER JOIN lineas_cesta as l ON l.producto_id = p.id";
        $sql .= " SET p.stock = $stock";
        $sql .= " WHERE l.id = {$this->getId()}";
        $edit = $this->db->query($sql);

        $result = false;

        if($edit) {
            $result = true;
        }

        return $result;
    }


    public function getLastInsert(){
        $ultima = $this->db->query("SELECT MAX(id) as id FROM lineas_cesta");
        return $ultima->fetch_object();
    }

}