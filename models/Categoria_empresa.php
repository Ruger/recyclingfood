<?php

class Categoria_empresa{
    // variables del moodelo Categoria_empresa
    private $id;
    private $nombre;
    private $db;

    public function __construct(){
        $this->db = Database::connect();
    }



    // getters

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }




    // setters

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $this->db->real_escape_string($nombre);
    }





    // metodos que relacionan con la base de datos


    public function getAll(){
        $categorias_empresas = $this->db->query("SELECT * FROM categorias_empresas ORDER BY id DESC");
        return $categorias_empresas;
    }





    public function save(){
        $sql = "INSERT INTO categorias_empresas VALUES (NULL, '{$this->getNombre()}')";
        $save = $this->db->query($sql);

        $result = false;

        if($save) {
            $result = true;
        }

        return $result;
    }





}