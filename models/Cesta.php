<?php

class Cesta{
    // variables del modelo Cesta
    private $id;
    private $cliente_id;
    private $estado_id;
    private $poblacion;
    private $direccion;
    private $descripcion;
    private $precio;
    private $fecha;
    private $hora;
    private $imagen;

    private $db;

    public function __construct(){
        $this->db = Database::connect();
    }


    // getters
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getClienteId()
    {
        return $this->cliente_id;
    }

    /**
     * @return mixed
     */
    public function getEstadoId()
    {
        return $this->estado_id;
    }

    /**
     * @return mixed
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }



    // setters
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $cliente_id
     */
    public function setClienteId($cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @param mixed $estado_id
     */
    public function setEstadoId($estado_id)
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @param mixed $poblacion
     */
    public function setPoblacion($poblacion)
    {
        $this->poblacion = $poblacion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }




    // metodos que relacionan con la base de datos
    public function save(){
        $sql = "INSERT INTO cestas VALUES (NULL, NULL, {$this->getEstadoId()}, '{$this->getPoblacion()}', '{$this->getDireccion()}', '{$this->getDescripcion()}', {$this->getPrecio()}, CURRENT_DATE, CURRENT_TIME, '{$this->getImagen()}')";
        $save = $this->db->query($sql);

        $result = false;

        if($save) {
            $result = true;
        }

        return $result;
    }


    public function delete(){
        $sql = "DELETE FROM cestas WHERE id = {$this->id}";
        $delete = $this->db->query($sql);

        $result = false;

        if($delete) {
            $result = true;
        }

        return $result;
    }



    public function edit(){
        $sql = "UPDATE cestas SET poblacion = '{$this->getPoblacion()}', direccion = '{$this->getDireccion()}', descripcion = '{$this->getDescripcion()}', precio = {$this->precio}";

        if($this->getImagen() !== null) {
            $sql .= ", imagen = '{$this->getImagen()}'";
        }

        $sql .= " WHERE id = {$this->getId()}";
        $edit = $this->db->query($sql);

        $result = false;

        if($edit) {
            $result = true;
        }

        return $result;
    }



    public function getAll(){
        $cestas = $this->db->query("SELECT * FROM cestas ORDER BY id DESC");
        return $cestas;
    }


    public function getAllAvailable(){
        $cestas = $this->db->query("SELECT * FROM cestas WHERE estado_id = 1 ORDER BY id DESC");
        return $cestas;
    }


    public function getAllFromCategory($id_categoria){
        $sql = "SELECT DISTINCT c.* FROM cestas as c";
        $sql .= " INNER JOIN lineas_cesta as l ON c.id = l.cesta_id";
        $sql .= " INNER JOIN productos as p ON l.producto_id = p.id";
        $sql .= " WHERE p.categoria_producto_id = $id_categoria";
        $sql .= " AND c.estado_id = 1";

        $cestas = $this->db->query($sql);
        return $cestas;
    }



    public function getAllFromClient(){
        $cestas = $this->db->query("SELECT * FROM cestas WHERE cliente_id = {$this->getClienteId()} ORDER BY fecha ASC");
        return $cestas;
    }



    public function getFresh($limit){
        $cestas = $this->db->query("SELECT * FROM cestas WHERE estado_id = 1 ORDER BY RAND(), fecha DESC LIMIT $limit");
        return $cestas;
    }



    public function getLastInsert(){
        $ultima = $this->db->query("SELECT MAX(id) as id FROM cestas");
        return $ultima->fetch_object();
    }


    public function getEnterprise(){
        $sql = "SELECT e.* FROM empresas as e ";
        $sql .= "INNER JOIN productos as p ON e.id = p.empresa_id ";
        $sql .= "INNER JOIN lineas_cesta as l ON p.id = l.producto_id ";
        $sql .= "WHERE l.cesta_id = {$this->getId()}";

        $empresa = $this->db->query($sql);
        return $empresa->fetch_object();
    }



    public function fetchById(){
        $cesta = $this->db->query("SELECT * FROM cestas WHERE id = {$this->getId()}");

        $cesta_fetch = $cesta->fetch_object();
        $cesta_objeto = new Cesta();
        $cesta_objeto->setId($cesta_fetch->id);
        $cesta_objeto->setClienteId($cesta_fetch->cliente_id);
        $cesta_objeto->setEstadoId($cesta_fetch->estado_id);
        $cesta_objeto->setPoblacion($cesta_fetch->poblacion);
        $cesta_objeto->setDireccion($cesta_fetch->direccion);
        $cesta_objeto->setDescripcion($cesta_fetch->descripcion);
        $cesta_objeto->setPrecio($cesta_fetch->precio);
        $cesta_objeto->setFecha($cesta_fetch->fecha);
        $cesta_objeto->setHora($cesta_fetch->hora);
        $cesta_objeto->setImagen($cesta_fetch->imagen);
        return $cesta_objeto;
    }


    public function getById(){
        $cesta = $this->db->query("SELECT * FROM cestas WHERE id = {$this->getId()}");
        return $cesta->fetch_object();
    }


    public function sell(){
        $sql = "UPDATE cestas SET cliente_id = {$this->getClienteId()}, estado_id = {$this->getEstadoId()}";
        $sql .= " WHERE id = {$this->getId()}";
        $sell = $this->db->query($sql);

        $result = false;

        if($sell) {
            $result = true;
        }

        return $result;
    }


    public function pay(){
        $sql = "UPDATE cestas SET estado_id = {$this->getEstadoId()}";
        $sql .= " WHERE id = {$this->getId()}";
        $pay = $this->db->query($sql);

        $result = false;

        if($pay) {
            $result = true;
        }

        return $result;
    }

}