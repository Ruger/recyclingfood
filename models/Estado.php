<?php

class Estado{
    // variables del modelo Estado
    private $id;
    private $nombre;

    private $db;

    public function __construct(){
        $this->db = Database::connect();
    }


    // getters
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }



    // setters
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }





    // metodos que relacionan con la base de datos
    public function getAll(){
        $estados = $this->db->query("SELECT * FROM estados ORDER BY id DESC");
        return $estados;
    }



    public function fetchById(){
        $estado = $this->db->query("SELECT * FROM estados WHERE id = {$this->getId()}");
        return $estado->fetch_object();
    }


}