<?php

class Categoria_producto{
    // variables del moodelo Categoria_empresa
    private $id;
    private $nombre;
    private $db;

    public function __construct(){
        $this->db = Database::connect();
    }



    // getters

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }




    // setters

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $this->db->real_escape_string($nombre);
    }





    // metodos que relacionan con la base de datos

    public function getAll(){
        $categorias_productos = $this->db->query("SELECT * FROM categorias_productos ORDER BY id DESC");
        return $categorias_productos;
    }


    public function getMenu(){
        $sql = "SELECT DISTINCT cp.* FROM categorias_productos as cp";
        $sql .= " INNER JOIN productos as p ON p.categoria_producto_id = cp.id";
        $sql .= " INNER JOIN lineas_cesta as l ON l.producto_id = p.id";
        $sql .= " INNER JOIN cestas as c ON c.id = l.cesta_id";
        $sql .= " WHERE c.estado_id = 1";
        $sql .= " ORDER BY cp.id DESC LIMIT 14";
        $categorias_productos = $this->db->query($sql);
        return $categorias_productos;
    }


    public function save(){
        $sql = "INSERT INTO categorias_productos VALUES (NULL, '{$this->getNombre()}')";
        $save = $this->db->query($sql);

        $result = false;

        if($save) {
            $result = true;
        }

        return $result;
    }



    public function fetchById(){
        $categoria = $this->db->query("SELECT * FROM categorias_productos WHERE id = {$this->getId()}");

        $categoria_fetch = $categoria->fetch_object();

        if(is_object($categoria_fetch)){
            $categoria_objeto = new Categoria_producto();
            $categoria_objeto->setId($categoria_fetch->id);
            $categoria_objeto->setNombre($categoria_fetch->nombre);

            return $categoria_objeto;
        }

    }




}