<?php

class Empresa{
    // variables del modelo Producto
    private $id;
    private $categoria_empresa_id;
    private $categoria_empresa_nombre;
    private $nombre;
    private $poblacion;
    private $email;
    private $password;
    private $imagen;

    private $db;

    public function __construct(){
        $this->db = Database::connect();
    }


    // getters

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCategoriaEmpresaId()
    {
        return $this->categoria_empresa_id;
    }


    /**
     * @return mixed
     */
    public function getCategoriaEmpresaNombre($id_categoria)
    {
        $this->setCategoriaEmpresaNombre($id_categoria);
        return $this->categoria_empresa_nombre;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }


    // setters
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $categoria_empresa_id
     */
    public function setCategoriaEmpresaId($categoria_empresa_id)
    {
        $this->categoria_empresa_id = $categoria_empresa_id;
    }


    /**
     * @param mixed $categoria_empresa_nombre
     */
    public function setCategoriaEmpresaNombre($id_categoria)
    {
        $sql = "SELECT * FROM categorias_empresas WHERE id = {$id_categoria}";
        $categoria = $this->db->query($sql);
        $categoria_objeto = $categoria->fetch_object();
        $this->categoria_empresa_nombre = $categoria_objeto->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param mixed $poblacion
     */
    public function setPoblacion($poblacion)
    {
        $this->poblacion = $poblacion;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }



    // metodos que relacionan con la base de datos
    public function save(){
        $sql = "INSERT INTO empresas VALUES (NULL, {$this->getCategoriaEmpresaId()}, '{$this->getNombre()}',  '{$this->getPoblacion()}', '{$this->getEmail()}', '{$this->getPassword()}', '{$this->getImagen()}')";
        $save = $this->db->query($sql);

        $result = false;

        if($save) {
            $result = true;
        }

        return $result;
    }


    public function getAll(){
        $empresas = $this->db->query("SELECT * FROM empresas ORDER BY id ASC");
        return $empresas;
    }


    public function getAllWithProducts(){
        $empresas = $this->db->query("SELECT e.* FROM empresas as e WHERE e.id IN (SELECT p.empresa_id FROM productos as p) ORDER BY id ASC");
        return $empresas;
    }


}