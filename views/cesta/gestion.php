<h1>Gestión de cestas</h1>

<?php while ($emp = $empresas->fetch_object()): ?>
    <span>
        <a href="<?php base_url ?>crear&empresa=<?php echo $emp->id ?>" class="button button-small">Crear cesta para <?php echo $emp->nombre ?></a>
    </span>
<?php endwhile; ?>


<!-- Gestion de errores en el proceso de creacion de cestas -->
<?php if(isset($_SESSION['cesta']) && $_SESSION['cesta'] == 'completed'): ?>
    <strong class="alert_green">La cesta se ha publicado correctamente</strong>

<?php elseif (isset($_SESSION['cesta']) && $_SESSION['cesta'] != 'completed'): ?>
    <strong class="alert_red">Ha ocurrido un fallo con publicación de la cesta</strong>

<?php endif; ?>
<?php Utilidades::deleteSession('cesta'); ?>

<!-- Gestion de errores en el proceso de eliminacion de cestas -->
<?php if(isset($_SESSION['delete']) && $_SESSION['delete'] == 'completed'): ?>
    <strong class="alert_green">La cesta se ha eliminado correctamente</strong>

<?php elseif (isset($_SESSION['delete']) && $_SESSION['delete'] != 'completed'): ?>
    <strong class="alert_red">Ha ocurrido un fallo a la hora de eliminar la cesta</strong>

<?php endif; ?>
<?php Utilidades::deleteSession('delete'); ?>

<table>
    <caption>Cestas</caption>
    <tr>
        <th>Identificador</th>
        <th>Empresa</th>
        <th>Precio</th>
        <th>Poblacion</th>
        <th>Direccion</th>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Cliente</th>
        <th>Estado</th>
        <th>Acciones</th>

    </tr>
    <?php while ($ces = $cestas->fetch_object()): ?>
        <tr>
            <td> <?php echo $ces->id; ?></td>
            <td> <?php echo Utilidades::showEmpresaDeCesta($ces->id)->nombre; ?></td>
            <td> <?php echo $ces->precio; ?></td>
            <td> <?php echo $ces->poblacion; ?></td>
            <td> <?php echo $ces->direccion; ?></td>
            <td> <?php echo $ces->fecha; ?></td>
            <td> <?php echo $ces->hora; ?></td>
            <td> <?php echo $ces->cliente_id; ?></td>
            <td> <?php echo Utilidades::showEstado($ces->estado_id)->nombre; ?></td>
            <?php if($ces->estado_id != 2 && $ces->estado_id != 3): ?>
                <td>
                    <a href="<?php echo base_url ?>cesta/editar&id=<?php echo $ces->id; ?>" class="button button-gestion">Editar</a>
                    <a href="<?php echo base_url ?>cesta/eliminar&id=<?php echo $ces->id; ?>" class="button button-gestion button-red">Eliminar</a>
                </td>

            <?php elseif($ces->estado_id == 2): ?>
                <td>
                    <a href="<?php echo base_url ?>cesta/pagar&id=<?php echo $ces->id; ?>" class="button button-gestion">Marcar como pagada</a>
                    <a style="visibility: hidden" href="#" class="button button-gestion button-red">Eliminar</a>
                </td>

            <?php else: ?>
                <td>
                    <a style="visibility: hidden" href="#" class="button button-gestion">Editar</a>
                    <a style="visibility: hidden" href="#" class="button button-gestion button-red">Eliminar</a>
                </td>

            <?php endif; ?>
        </tr>
    <?php endwhile; ?>
</table>
