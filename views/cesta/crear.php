<?php  if (isset($edit, $cest) && is_object($cest)): ?>
    <h1>Editar cesta: <?php echo $cest->getId() ?> (<?php echo $empresa->nombre ?>)</h1>
    <?php $url_action = base_url . 'cesta/save&id=' . $cest->getId(); ?>
    <?php $editar = true; ?>

<?php else: ?>
    <h1>Crear nueva cesta</h1>
    <?php $url_action = base_url . 'cesta/save'; ?>
    <?php $editar = false; ?>

<?php endif; ?>

<div class="form_container">
    <form action="<?php echo $url_action ?>" method="post" enctype="multipart/form-data">

        <?php while ($producto = $productos->fetch_object()): ?>
            <div class="product_container">

             <!-- Carga la imagen del producto -->
            <?php if(isset($producto) && is_object($producto) && !empty($producto->imagen)): ?>
                <img class="thumb crear_cesta" src="<?php echo base_url ?>uploads/images/<?php echo $producto->imagen ?>">

            <!-- O una imagen por defecto, si el producto no tiene imagen -->
            <?php else: ?>
                <img class="thumb crear_cesta" src="<?php echo base_url ?>assets/img/default.jpg">

            <?php endif; ?>

                <?php
                    $value = 0;
                    $stock = $producto->stock;

                    if($editar){
                        while ($lin = $lineas->fetch_object()){
                            if($lin->producto_id == $producto->id){
                                $value = $lin->cantidad;
                                $stock = $lin->cantidad + $producto->stock;
                            }
                        }
                    }

                ?>
                <span><?php echo $producto->nombre ?></span>
                <input class="product_number" name="<?php echo $producto->id ?>" type="number" min="0" max="<?php echo $stock ?>" value="<?php echo $value ?>">
                <span><?php echo $producto->precio ?> €/u</span>

            </div>
        <?php if($editar){ $lineas = Utilidades::showLineasCesta($cest->getId()); } ?>
        <?php endwhile; ?>

        <label for="poblacion">Poblacion</label>
        <input type="text" name="poblacion" required value="<?php echo isset($cest) && is_object($cest) ? $cest->getPoblacion() : '' ?>"/>

        <label for="direccion">Direccion</label>
        <input type="text" name="direccion" required value="<?php echo isset($cest) && is_object($cest) ? $cest->getDireccion() : '' ?>"/>

        <label for="descripcion">Descripción</label>
        <textarea name="descripcion"><?php echo isset($cest) && is_object($cest) ? $cest->getDescripcion() : '' ?></textarea>

        <label for="imagen">Imagen</label>
        <?php if(isset($cest) && is_object($cest) && !empty($cest->getImagen())): ?>
            <img class="thumb" src="<?php echo base_url ?>uploads/images/<?php echo $cest->getImagen() ?>">
        <?php endif; ?>

        <input type="file" name="imagen"/>

        <input type="submit" value="Guardar" />
    </form>
</div>