<?php if(isset($cest) and isset($empresa)): ?>
    <h1>Cesta de <?php echo $empresa->nombre; ?></h1>

    <div class="detail-product">
        <div class="image">
            <!-- Carga la imagen de la cesta -->
            <?php if(isset($cest) && is_object($cest) && !empty($cest->getImagen())): ?>
                <img src="<?php echo base_url ?>uploads/images/<?php echo $cest->getImagen() ?>">

            <!-- O una imagen por defecto, si la cesta no tiene imagen -->
            <?php else: ?>
                <img src="<?php echo base_url ?>assets/img/cesta_default.jpg">

            <?php endif; ?>
        </div>

        <div class="data">
            <p class="description"><?php echo $cest->getDescripcion() ?></p>


            <?php while ($linea = $lineas->fetch_object()): ?>
                <?php $producto = Utilidades::showProducto($linea->producto_id) ?>
                <?php if($linea->cantidad > 0): ?>
                    <div class="detail-lines">
                        <p class="line-data line-name"><?php echo $producto->nombre ?></p>
                        <span class="line-data line-price"><?php echo $producto->precio ?> € </span>
                        <span class="line-data line-x">x</span>
                        <span class="line-data line-cuantity"><?php echo $linea->cantidad ?> unidades</span>
                    </div>
                <?php endif; ?>
            <?php endwhile;?>


            <p class="price"><?php echo $cest->getPrecio() ?> €</p>

            <?php if (!Utilidades::isInCart($cest->getId()) && Utilidades::isAvailable($cest->getId())): ?>
                <a href="<?php echo base_url ?>carrito/add&id=<?php echo $cest->getId() ?>" class="button">Salvar</a>


            <?php elseif(Utilidades::isBought($cest->getId())): ?>
                <a href="<?php echo base_url ?>pedido/realizados" class="button">Ver en mis cestas</a>


            <?php elseif(!Utilidades::isAvailable($cest->getId())): ?>
                <a href="#" class="button">No disponible</a>


            <?php else: ?>
                <a href="<?php echo base_url ?>carrito/index" class="button">Ver en el carrito</a>

            <?php endif; ?>

        </div>
    </div>



<?php else: ?>
    <h1>La cesta no existe</h1>

<?php endif; ?>
