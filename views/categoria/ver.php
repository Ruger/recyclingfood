<?php if(isset($categoria)): ?>
    <h1><?php echo $categoria->getNombre(); ?></h1>

    <?php if($cestas->num_rows == 0): ?>
        <h3><p>No hay cestas de esta categoría para mostrar</p></h3>

    <?php else: ?>
        <?php while ($ces = $cestas->fetch_object()): ?>
            <?php
            $cest = new Cesta();
            $cest->setId($ces->id);
            $cest = $cest->fetchById();
            ?>

            <div class="product">
                <!-- Enlace a la cesta -->
                <a href="<?php echo base_url ?>cesta/ver&id=<?php echo $cest->getId() ?>">
                    <!-- Carga la imagen de la cesta -->
                    <?php if(isset($cest) && is_object($cest) && !empty($cest->getImagen())): ?>
                        <img class="thumb" src="<?php echo base_url ?>uploads/images/<?php echo $cest->getImagen() ?>">

                        <!-- O una imagen por defecto, si la cesta no tiene imagen -->
                    <?php else: ?>
                        <img class="thumb" src="<?php echo base_url ?>assets/img/cesta_default.jpg">

                    <?php endif; ?>

                    <h2><?php echo $cest->getEnterprise()->nombre ?> </h2>
                </a>

                <p><?php echo $ces->precio ?> €</p>

                <?php if (!Utilidades::isInCart($cest->getId()) && Utilidades::isAvailable($cest->getId())): ?>
                    <a href="<?php echo base_url ?>carrito/add&id=<?php echo $cest->getId() ?>" class="button">Salvar</a>


                <?php elseif(!Utilidades::isAvailable($cest->getId())): ?>
                    <a href="#" class="button">No disponible</a>

                <?php else: ?>
                    <a href="<?php echo base_url ?>carrito/index" class="button">Ver en el carrito</a>

                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>

<?php else: ?>
    <h1>La categoria no existe</h1>

<?php endif; ?>
