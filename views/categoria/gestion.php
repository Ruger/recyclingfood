<h1>Gestionar categorias</h1>

<a href="<?php base_url ?>crear_categoria_producto" class="button button-small">Crear categoria de productos</a>
<a href="<?php base_url ?>crear_categoria_empresa" class="button button-small">Crear categoria de empresas</a>

<table>
    <caption>Categorías de productos</caption>
    <tr>
        <th>Identificador</th>
        <th>Nombre</th>
    </tr>
    <?php while ($cat_producto = $categorias_productos->fetch_object()): ?>
        <tr>
            <td><a href="<?php echo base_url ?>categoria/ver&id=<?php echo $cat_producto->id ?>"><?php echo $cat_producto->id; ?></a></td>
            <td><?php echo $cat_producto->nombre; ?></td>
        </tr>
    <?php endwhile; ?>
</table>


<table>
    <caption>Categorías de empresas</caption>
    <tr>
        <th>Identificador</th>
        <th>Nombre</th>
    </tr>
    <?php while ($cat_empresa = $categorias_empresas->fetch_object()): ?>
        <tr>
            <td><?php echo $cat_empresa->id; ?></td>
            <td><?php echo $cat_empresa->nombre; ?></td>
        </tr>
    <?php endwhile; ?>
</table>