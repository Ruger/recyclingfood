<h1>Registrarse</h1>

<!-- TODO Poner las alarmas de registro correcto / incorrecto de forma que desaparezcan solas tras unos segundos, ¿quizas JavasCript? -->

<?php if(isset($_SESSION['register']) && $_SESSION['register'] === 'completed') : ?>
    <strong class="alert_green">Registro completado correctamente</strong>

<?php elseif (isset($_SESSION['register']) && $_SESSION['register'] === 'failed'): ?>
    <strong class="alert_red">Registro fallido, trata de introducir bien los datos</strong>

<?php endif;?>

<?php Utilidades::deleteSession('register'); ?>

<form action="<?php echo base_url?>cliente/save" method="POST">
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" required />

    <label for="apellidos">Apellidos</label>
    <input type="text" name="apellidos" required />

    <label for="poblacion">Población</label>
    <input type="text" name="poblacion" required />

    <label for="email">Email</label>
    <input type="email" name="email" required />

    <label for="password">Contraseña</label>
    <input type="password" name="password" required />

    <input type="submit" value="Registrarse" />
</form>