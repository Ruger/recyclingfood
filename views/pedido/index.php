<?php if (isset($_SESSION['client']) && isset($_SESSION['carrito'])):?>
    <?php $stats = Utilidades::statsCarrito();?>
    <h1>Realizar pedido: <?php echo $stats['total'] ?> €</h1>


    <h3>Dirección para la recogida de las cestas:</h3>
    <?php ?>

    <?php foreach ($carrito as $elemento):?>
        <?php $cesta = $elemento['cesta']; ?>
        <?php $empresa = Utilidades::showEmpresaDeCesta($cesta->id);?>

        <div class="cesta-pedido">
            <!-- Carga la imagen de la cesta -->
            <?php if(isset($cesta) && is_object($cesta) && !empty($cesta->imagen)): ?>
                <a href="<?php echo base_url ?>cesta/ver&id=<?php echo $cesta->id ?>"><img src="<?php echo base_url ?>uploads/images/<?php echo $cesta->imagen ?>" class="img_carrito"></a>

                <!-- O una imagen por defecto, si la cesta no tiene imagen -->
            <?php else: ?>
                <a href="<?php echo base_url ?>cesta/ver&id=<?php echo $cesta->id ?>"><img src="<?php echo base_url ?>assets/img/cesta_default.jpg" class="img_carrito"></a>

            <?php endif; ?>

            <h5 class="enterprise-pedido"><?php echo $empresa->nombre ?></h5>

            <label for="poblacion">Poblacion</label>
            <input type="text" name="poblacion" value="<?php echo $cesta->poblacion ?>" disabled>

            <label for="direccion">Dirección</label>
            <input type="text" name="direccion" value="<?php echo $cesta->direccion ?>" disabled>
        </div>

    <?php endforeach; ?>

    <div class="buttons-pedido">
        <a class="volver-carrito" href="<?php echo base_url?>carrito/index">Volver al carrito</a>
        <a class="button button-pedido" href="<?php echo base_url?>pedido/add">Confirmar pedido</a>
    </div>


<?php else: ?>
    <h1>¡Ups!</h1>
    <p>Necesitas estar registrado y logueado en Recyling Food para poder realizar tu pedido</p>

<?php endif; ?>



