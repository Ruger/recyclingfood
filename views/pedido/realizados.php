<h1>Mis cestas</h1>

<table>
    <tr>
        <th>Nº cesta</th>
        <th>Fecha</th>
        <th>Coste</th>
        <th>Empresa</th>
        <th>Localidad</th>
        <th>Dirección</th>
        <th>Estado</th>
    </tr>

    <?php while ($cesta = $cestas->fetch_object()): ?>
        <?php $empresa = Utilidades::showEmpresaDeCesta($cesta->id); ?>

        <tr>
            <td><a href="<?php echo base_url ?>cesta/ver&id=<?php echo $cesta->id ?>"><?php echo $cesta->id ?></a></td>
            <td><?php echo $cesta->fecha ?></td>
            <td><?php echo $cesta->precio ?> €</td>
            <td><?php echo $empresa->nombre ?></td>
            <td><?php echo $cesta->poblacion ?></td>
            <td><?php echo $cesta->direccion ?></td>
            <td><?php echo Utilidades::showEstado($cesta->estado_id)->nombre ?></td>
        </tr>

    <?php  endwhile; ?>

</table>