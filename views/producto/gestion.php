<h1>Gestión de productos</h1>

<a href="<?php base_url ?>crear" class="button button-small">Crear producto</a>

<!-- Gestion de errores en el proceso de creacion de productos -->
<?php if(isset($_SESSION['producto']) && $_SESSION['producto'] == 'completed'): ?>
    <strong class="alert_green">El producto se ha creado correctamente</strong>

<?php elseif (isset($_SESSION['producto']) && $_SESSION['producto'] != 'completed'): ?>
    <strong class="alert_red">Ha ocurrido un fallo con la creación del producto</strong>

<?php endif; ?>
<?php Utilidades::deleteSession('producto'); ?>

<!-- Gestion de errores en el proceso de eliminacion de productos -->
<?php if(isset($_SESSION['delete']) && $_SESSION['delete'] == 'completed'): ?>
    <strong class="alert_green">El producto se ha eliminado correctamente</strong>

<?php elseif (isset($_SESSION['delete']) && $_SESSION['delete'] != 'completed'): ?>
    <strong class="alert_red">Ha ocurrido un fallo a la hora de eliminar el producto</strong>

<?php endif; ?>
<?php Utilidades::deleteSession('delete'); ?>

<table>
    <caption>Productos</caption>
    <tr>
        <th>Identificador</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Stock</th>
        <th>Acciones</th>
    </tr>
    <?php while ($pro = $productos->fetch_object()): ?>
        <tr>
            <td> <?php echo $pro->id; ?></td>
            <td> <?php echo $pro->nombre; ?></td>
            <td> <?php echo $pro->precio; ?></td>
            <td> <?php echo $pro->stock; ?></td>
            <td>
                <a href="<?php base_url ?>editar&id=<?php echo $pro->id; ?>" class="button button-gestion">Editar</a>
                <a href="<?php base_url ?>eliminar&id=<?php echo $pro->id; ?>" class="button button-gestion button-red">Eliminar</a>
            </td>
        </tr>
    <?php endwhile; ?>
</table>
