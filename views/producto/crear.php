<?php  if (isset($edit, $product) && is_object($product)): ?>
    <h1>Editar producto: <?php echo $product->nombre ?></h1>
    <?php $url_action = base_url . 'producto/save&id=' . $product->id; ?>

<?php else: ?>
    <h1>Crear nuevo producto</h1>
    <?php $url_action = base_url . 'producto/save'; ?>

<?php endif; ?>


<div class="form_container">
    <form action="<?php echo $url_action ?>" method="post" enctype="multipart/form-data">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" required value="<?php echo isset($product) && is_object($product) ? $product->nombre : '' ?>"/>

        <label for="descripcion">Descripción</label>
        <textarea name="descripcion"><?php echo isset($product) && is_object($product) ? $product->descripcion : '' ?></textarea>

        <label for="precio">Precio</label>
        <input class="numero" type="text" name="precio" required value="<?php echo isset($product) && is_object($product) ? $product->precio : '' ?>"/>

        <label for="stock">Stock</label>
        <input class="numero" type="number" name="stock" required value="<?php echo isset($product) && is_object($product) ? $product->stock : '' ?>"/>

        <label for="categoria_producto">Categoria</label>
        <?php $categorias = Utilidades::showCategoriasProductos()?>
        <select name="categoria_producto" required>
            <?php while ($cat = $categorias->fetch_object()): ?>
                <option value="<?php echo $cat->id ?>" <?php echo isset($product) && is_object($product) && $cat->id === $product->categoria_producto_id ? 'selected' : '' ?>>
                  <?php echo $cat->nombre ?>
                </option>
            <?php endwhile; ?>
        </select>

        <label for="empresa">Empresa</label>
        <?php $empresas = Utilidades::showEmpresas()?>
        <select name="empresa" required>
            <?php while ($emp = $empresas->fetch_object()): ?>
                <option value="<?php echo $emp->id ?>" <?php echo isset($product) && is_object($product) && $emp->id === $product->empresa_id ? 'selected' : '' ?>>
                    <?php echo $emp->nombre ?>
                </option>
            <?php endwhile; ?>
        </select>

        <label for="imagen">Imagen</label>
        <?php if(isset($product) && is_object($product) && !empty($product->imagen)): ?>
            <img class="thumb" src="<?php echo base_url ?>uploads/images/<?php echo $product->imagen ?>">
        <?php endif; ?>

        <input type="file" name="imagen"/>


        <input type="submit" value="Guardar" />
    </form>
</div>