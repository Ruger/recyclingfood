<h1>Gestión de empresas</h1>

<a href="<?php base_url ?>crear" class="button button-small">Crear empresa</a>

<table>
    <caption>Empresas</caption>
    <tr>
        <th>Identificador</th>
        <th>Nombre</th>
        <th>Categoria</th>
        <th>Poblacion</th>
        <th>Email</th>
    </tr>
    <?php while ($emp = $empresas->fetch_object()): ?>
        <tr>
            <td> <?php echo $emp->id; ?></td>
            <td> <?php echo $emp->nombre; ?></td>
            <td> <?php echo Utilidades::showNombreCategoria($emp->categoria_empresa_id) ?></td>
            <td> <?php echo $emp->poblacion; ?></td>
            <td> <?php echo $emp->email; ?></td>
        </tr>
    <?php endwhile; ?>
</table>
