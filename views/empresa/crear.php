<h1>Crear nueva empresa</h1>

<div class="form_container">
    <form action="<?php echo base_url?>empresa/save" method="post">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" required/>

        <label for="poblacion">Poblacion</label>
        <input type="text" name="poblacion" required/>

        <label for="email">Email</label>
        <input type="email" name="email" required/>

        <label for="password">Contraseña</label>
        <input type="password" name="password" required/>

        <label for="categoria">Categoria</label>
        <?php $categorias = Utilidades::showCategoriasEmpresas()?>
        <select name="categoria" required>
            <?php while ($cat = $categorias->fetch_object()): ?>
                <option value="<?php echo $cat->id ?>">
                  <?php echo $cat->nombre ?>
                </option>
            <?php endwhile; ?>
        </select>

        <label for="imagen">Imagen</label>
        <input type="file" name="imagen"/>


        <input type="submit" value="Guardar" />
    </form>
</div>