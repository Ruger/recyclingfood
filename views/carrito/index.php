<h1>Carrito de la compra</h1>
<?php if (isset($carrito)): ?>
    <table>
        <tr>
            <th>Imagen</th>
            <th>Empresa</th>
            <th>Precio</th>
            <th>Eliminar del carrito</th>
        </tr>
        <?php foreach ($carrito as $indice => $elemento):?>
            <?php
                $cesta =  $elemento['cesta'];
                $empresa = $elemento['empresa'];
            ?>

            <tr>
                <td>
                    <!-- Carga la imagen de la cesta -->
                    <?php if(isset($cesta) && is_object($cesta) && !empty($cesta->imagen)): ?>
                        <a href="<?php echo base_url ?>cesta/ver&id=<?php echo $cesta->id ?>"><img src="<?php echo base_url ?>uploads/images/<?php echo $cesta->imagen ?>" class="img_carrito"></a>

                    <!-- O una imagen por defecto, si la cesta no tiene imagen -->
                    <?php else: ?>
                        <a href="<?php echo base_url ?>cesta/ver&id=<?php echo $cesta->id ?>"><img src="<?php echo base_url ?>assets/img/cesta_default.jpg" class="img_carrito"></a>

                    <?php endif; ?>
                </td>

                <td>
                    <?php echo $empresa->nombre ?>
                </td>

                <td>
                    <?php echo $cesta->precio ?>
                </td>

                <td>
                    <a class="button button-red button-small" href="<?php echo base_url ?>carrito/delete&id=<?php echo $cesta->id ?>">Quitar cesta</a>
                </td>

            </tr>
        <?php endforeach; ?>
    </table>

    <a class="button button-vaciar button-red" href="<?php echo base_url ?>carrito/delete_all">Vaciar el carrito</a>

    <?php $stats = Utilidades::statsCarrito(); ?>
    <div class="total-carrito">
        <h3>Precio total: <?php echo $stats['total']; ?> €</h3>
        <a class="button button-pedido" href="<?php echo base_url ?>pedido/index">Realizar pedido</a>
    </div>


    <?php else: ?>
        <h2>El carrito está vacío. ¡Vamos a salvar algo!</h2>

    <?php endif; ?>

