<!DOCTYPE HTML>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title>Recycling Food</title>
    <link rel="stylesheet" href="<?php echo base_url ?>assets/css/styles.css"/>
    <link rel="icon" href="<?php echo base_url ?>assets/img/campaigns-network-project-logo.png" type="image/png"/>
</head>

<body>
<div id="container">
    <!-- Cabecera -->
    <header id="header">
        <div id="logo">
            <img src="<?php echo base_url ?>assets/img/campaigns-network-project-logo.png" alt="Logo Recycling Food">
            <a href="<?php echo base_url ?>">
                Recycling Food
            </a>
        </div>
    </header>

    <!-- Menu navegacion (categorias) -->
    <?php $categorias_productos = Utilidades::showMenuCategoriasProductos(); ?>
    <nav id="menu">
        <ul>
            <li>
                <a href="<?php echo base_url ?>">Inicio</a>
            </li>

            <?php while ($cat_producto = $categorias_productos->fetch_object()): ?>
                <li>
                    <a href="<?php echo base_url ?>categoria/ver&id=<?php echo $cat_producto->id ?>"><?php echo $cat_producto->nombre ?></a>
                </li>
            <?php endwhile; ?>
        </ul>
    </nav>

    <div id="content">