<!-- Barra lateral -->
<aside id="lateral">

    <div id="carrito" class="block_aside">
        <h3>Mi carrito</h3>
        <ul>
            <?php $stats = Utilidades::statsCarrito(); ?>
            <li>Total: <?php echo $stats['total'] ?> €</li>
            <li><a href="<?php echo base_url ?>carrito/index">Ver el carrito (<?php echo $stats['count'] === 1 ? $stats['count'] . ' cesta' : $stats['count'] . ' cestas'; ?>)</a></li>
        </ul>
    </div>


    <div id="login" class="block_aside">

        <?php if(!isset($_SESSION['client'])): ?>
        <h3>Entrar a la tienda</h3>
        <form action="<?php echo base_url ?>cliente/login" method="post">
            <label for="email">Email</label>
            <input type="email" name="email"/>

            <label for="password">Contraseña</label>
            <input type="password" name="password"/>

            <input type="submit" value="Entrar"/>
        </form>
        <?php else: ?>
            <h3><?php echo 'Bienvenido ' . $_SESSION['client']->nombre . ' ' . $_SESSION['client']->apellidos ?></h3>
        <?php endif; ?>


        <ul>
            <?php if(isset($_SESSION['admin']) === true && $_SESSION['client']->rol === 'admin'): ?>
                <li><a href="<?php echo base_url ?>cesta/gestion">Gestionar cestas</a></li>
                <li><a href="<?php echo base_url ?>producto/gestion">Gestionar productos</a></li>
                <li><a href="<?php echo base_url ?>empresa/gestion">Gestionar empresas</a></li>
                <li><a href="<?php echo base_url ?>categoria/gestion">Gestionar categorias</a></li>
            <?php endif; ?>

            <?php if(isset($_SESSION['client'])): ?>
                <li><a href="<?php echo base_url ?>pedido/realizados">Mis cestas</a></li>
                <li><a href="<?php echo base_url ?>cliente/logout">Cerrar sesión</a></li>

            <?php else: ?>
                <li><a href="<?php echo base_url ?>cliente/registro">Registrate</a></li>
            <?php endif; ?>
        </ul>

    </div>

</aside>


<!-- Contenido central (productos) -->
<div id="central">