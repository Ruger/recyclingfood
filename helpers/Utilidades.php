<?php

// Clase con funciones estaticas para poder ser llamadas cuando sea necesario realizar acciones generales

class Utilidades {

    public static function deleteSession($name){
        if(isset($_SESSION[$name])) {
            $_SESSION[$name] = null;
            unset($_SESSION[$name]);
        }
        return $name;
    }


    public static function isAdmin(){
        if(!isset($_SESSION['admin']) || $_SESSION['admin'] !== true){
            header("Location:" . base_url);

        }else{
            return true;
        }
    }



    public static function isClient(){
        if(!isset($_SESSION['client'])){
            header("Location:" . base_url);

        }else{
            return true;
        }
    }


    public static function isInCart($id_cesta){
        if(isset($_SESSION['carrito'])){
            $counter = 0;
            foreach ($_SESSION['carrito'] as $indice => $elemento){
                if($elemento['cesta_id'] == $id_cesta){
                    $counter++;
                }
            }

            if($counter == 0){
                return false;

            }else{
                return true;
            }

        }else{
            return false;
        }
    }



    public static function isBought($id_cesta){
        require_once 'models/Cesta.php';
        $cesta = new Cesta();
        $cesta->setId($id_cesta);
        $cesta = $cesta->fetchById();

        $id_cliente = $_SESSION['client']->id;
        if($cesta->getClienteId() == $id_cliente){
            return true;

        }else{
            return false;
        }
    }



    public static function isAvailable($id_cesta){
        require_once 'models/Cesta.php';
        $cesta = new Cesta();
        $cesta->setId($id_cesta);
        $cesta = $cesta->fetchById();

        if($cesta->getEstadoId() == 1){
            return true;

        }else{
            return false;
        }
    }



    public static function showCategoriasEmpresas(){
        require_once 'models/Categoria_empresa.php';
        $categoria_empresa = new Categoria_empresa();
        $categorias_empresas = $categoria_empresa->getAll();

        return $categorias_empresas;
    }



    public static function showCategoriasProductos(){
        require_once 'models/Categoria_producto.php';
        $categoria_producto = new Categoria_producto();
        $categorias_productos = $categoria_producto->getAll();

        return $categorias_productos;
    }



    public static function showMenuCategoriasProductos(){
        require_once 'models/Categoria_producto.php';
        $categoria_producto = new Categoria_producto();
        $categorias_productos = $categoria_producto->getMenu();

        return $categorias_productos;
    }



    public static function showEmpresas(){
        require_once 'models/Empresa.php';
        $empresa = new Empresa();
        $empresas = $empresa->getAll();

        return $empresas;
    }



    public static function showEmpresasConProductos(){
        require_once 'models/Empresa.php';
        $empresa = new Empresa();
        $empresas = $empresa->getAllWithProducts();

        return $empresas;
    }



    public static function showEmpresaDeCesta($id_cesta){
        require_once 'models/Cesta.php';
        $cesta = new Cesta();
        $cesta->setId($id_cesta);
        $empresa = $cesta->getEnterprise();

        return $empresa;
    }



    public static function showProductos(){
        require_once 'models/Producto.php';
        $producto = new Producto();
        $productos = $producto->getAll();

        return $productos;
    }



    public static function showProductosDeEmpresa($id_empresa){
        require_once 'models/Producto.php';
        $producto = new Producto();
        $producto->setEmpresaId($id_empresa);
        $productos = $producto->getAllFromEnterprise();

        return $productos;
    }



    public static function showProducto($id_producto){
        require_once 'models/Producto.php';
        $producto = new Producto();
        $producto->setId($id_producto);
        $product = $producto->fetchById();

        return $product;
    }



    public static function showNombreCategoria($id_categoria){
        require_once 'models/Empresa.php';
        $empresa = new Empresa();
        $nombre_categoria = $empresa->getCategoriaEmpresaNombre($id_categoria);

        return $nombre_categoria;
    }



    public static function showEstado($id_estado){
        require_once 'models/Estado.php';
        $estado = new Estado();
        $estado->setId($id_estado);
        $estad = $estado->fetchById();

        return $estad;
    }



    public static function showUltimaCesta(){
        require_once 'models/Cesta.php';
        $cesta = new Cesta();
        $identificador = $cesta->getLastInsert();
        $cesta->setId($identificador->id);
        return $cesta->fetchById();
    }



    public static function showUltimaLineaCesta(){
        require_once 'models/Linea_cesta.php';
        $linea = new Linea_cesta();
        $identificador = $linea->getLastInsert();
        $linea->setId($identificador->id);
        return $linea->fetchById();
    }



    public static function showLineasCesta($id_cesta){
        require_once 'models/Linea_cesta.php';
        $linea = new Linea_cesta();
        $linea->setCestaId($id_cesta);
        $lineas = $linea->fetchByCesta();

        return $lineas;
    }



    public static function showCestasDeCliente($id_cliente){
        require_once 'models/Cesta.php';
        $cesta = new Cesta();
        $cesta->setClienteId($id_cliente);
        $cestas = $cesta->getAllFromClient();

        return $cestas;
    }



    public static function sellCesta($id_cesta, $id_cliente){
        require_once 'models/Cesta.php';
        $cesta = new Cesta();
        $cesta->setId($id_cesta);
        $cesta->setClienteId($id_cliente);
        $cesta->setEstadoId(2);

        return $cesta->sell();
    }



    public static function statsCarrito(){
        $stats = array(
            'count' => 0,
            'total' => 0
        );

        if(isset($_SESSION['carrito'])){
            $stats['count'] = count($_SESSION['carrito']);

            foreach ($_SESSION['carrito'] as $elemento){
                $stats['total'] += $elemento['cesta']->precio;
            }

        }

        return $stats;
    }



}