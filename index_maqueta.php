<!DOCTYPE HTML>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title>Recycling Food</title>
    <link rel="stylesheet" href="assets/css/styles.css"/>
    <link  rel="icon"   href="assets/img/campaigns-network-project-logo.png" type="image/png" />
</head>

<body>
<div id="container">
    <!-- Cabecera -->
    <header id="header">
        <div id="logo">
            <img src="assets/img/campaigns-network-project-logo.png" alt="Logo Recycling Food">
            <a href="index_maqueta.php">
                Recycling Food
            </a>
        </div>
    </header>

    <!-- Menu navegacion (categorias) -->
    <nav id="menu">
        <ul>
            <li>
                <a href="#">Inicio</a>
            </li>

            <li>
                <a href="#">Categoria 1</a>
            </li>

            <li>
                <a href="#">Categoria 2</a>
            </li>

            <li>
                <a href="#">Categoria 3</a>
            </li>
        </ul>
    </nav>

    <div id="content">
        <!-- Barra lateral -->
        <aside id="lateral">

            <div id="login" class="block_aside">
                <h3>Entrar a la tienda</h3>
                <form action="#" method="post">
                    <label for="email">Email</label>
                    <input type="email" name="email"/>

                    <label for="password">Contraseña</label>
                    <input type="password" name="password"/>

                    <input type="submit" value="Entrar"/>
                </form>

                <ul>
                    <li><a href="#">Mis cestas</a></li>
                    <li><a href="#">Gestionar cestas</a></li>
                    <li><a href="#">Gestionar categorias</a></li>
                </ul>

            </div>

        </aside>


        <!-- Contenido central (productos) -->
        <div id="central">
            <h1>Lo más fresco del momento</h1>

            <div class="product">
                <img src="assets/img/manzana.jpg"/>
                <h2>Manzana roja 1</h2>
                <p>1 euro</p>
                <a href="#" class="button">Salvar</a>
            </div>

            <div class="product">
                <img src="assets/img/manzana.jpg"/>
                <h2>Manzana roja 2</h2>
                <p>1 euro</p>
                <a href="#" class="button">Salvar</a>
            </div>

            <div class="product">
                <img src="assets/img/manzana.jpg"/>
                <h2>Manzana roja 3</h2>
                <p>1 euro</p>
                <a href="#" class="button">Salvar</a>
            </div>


            <div class="product">
                <img src="assets/img/manzana.jpg"/>
                <h2>Manzana roja 4</h2>
                <p>1 euro</p>
                <a href="#" class="button">Salvar</a>
            </div>

        </div>

    </div> <!-- div content -->


    <!-- Footer -->
    <footer id="footer">
        <p>Desarrollado por Raúl Oriol y Rubén Rüger &copy; <?php echo date('Y'); ?></p>
    </footer>

</div>
</body>


</html>