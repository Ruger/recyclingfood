<?php

class PedidoController{

    public function index(){
        if(isset($_SESSION['carrito'])){
            $carrito = $_SESSION['carrito'];
        }

        require_once 'views/pedido/index.php';
    }



    public function add(){
        if(isset($_SESSION['carrito'])){
            $carrito = $_SESSION['carrito'];
            $id_usuario = $_SESSION['client']->id;
            $errores = 0;

            foreach ($carrito as $elemento){
                $cesta = $elemento['cesta'];
                $sell = Utilidades::sellCesta($cesta->id, $id_usuario);

                if(!$sell){
                    $errores++;
                }
            }

            if($errores == 0){
                $_SESSION['pedido'] = 'completed';
                Utilidades::deleteSession('carrito');


            }else{
                $_SESSION['pedido'] = 'failed';
            }

        }else{
            $_SESSION['pedido'] = 'failed';
        }


        header("Location:" . base_url . "pedido/confirmacion");
    }



    public function confirmacion(){
        require_once 'views/pedido/confirmacion.php';
    }



    public function realizados(){
        Utilidades::isClient();
        $id = $_SESSION['client']->id;

        // se sacan todas las cestas compradas por el usuario
        $cestas = Utilidades::showCestasDeCliente($id);
        require_once 'views/pedido/realizados.php';
    }




}