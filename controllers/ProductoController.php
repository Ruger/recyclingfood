<?php
require_once 'models/Producto.php';
class ProductoController{

    public function gestion(){
        Utilidades::isAdmin();

        $producto = new Producto();
        $productos = $producto->getAll();

        require_once 'views/producto/gestion.php';
    }


    public function crear(){
        Utilidades::isAdmin();
        require_once 'views/producto/crear.php';
    }


    public function save(){
        Utilidades::isAdmin();
        if(isset($_POST['nombre'])){
            $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
            $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : false;
            $precio = isset($_POST['precio']) ? $_POST['precio'] : false;
            $stock = isset($_POST['stock']) ? $_POST['stock'] : false;
            $categoria_producto = isset($_POST['categoria_producto']) ? $_POST['categoria_producto'] : false;
            $empresa = isset($_POST['empresa']) ? $_POST['empresa'] : false;

            if($nombre && $precio && $stock && $categoria_producto && $empresa){
                $producto = new Producto();
                $producto->setNombre($nombre);
                $producto->setDescripcion($descripcion);
                $producto->setPrecio($precio);
                $producto->setStock($stock);
                $producto->setCategoriaProductoId($categoria_producto);
                $producto->setEmpresaId($empresa);

                // guardar la imagen
                if(isset($_FILES['imagen'])){
                    $file = $_FILES['imagen'];
                    $filename = $file['name'];
                    $mimetype = $file['type'];

                    if($mimetype === 'image/jpg' || $mimetype == 'image/jpeg' || $mimetype === 'image/png' || $mimetype === 'image/gif'){
                        if(!is_dir('uploads/images')){
                            if (!mkdir('uploads/images', 0777, true) && !is_dir('uploads/images')) {
                                throw new \RuntimeException(sprintf('Directory "%s" was not created', 'uploads/images'));
                            }
                        }

                        move_uploaded_file($file['tmp_name'], 'uploads/images/' . $filename);
                        $producto->setImagen($filename);
                    }
                }


                if(isset($_GET['id'])){
                    // editar el producto
                    $id = $_GET['id'];
                    $producto->setId($id);
                    $save = $producto->edit();

                }else{
                    // crear el producto
                    $save = $producto->save();
                }


                if($save){
                    $_SESSION['producto'] = 'completed';

                }else{
                    $_SESSION['producto'] = 'failed';
                }

            }else{
                $_SESSION['producto'] = 'failed';
            }

        }else{
            $_SESSION['producto'] = 'failed';
        }

        header('Location:' . base_url . 'producto/gestion');
    }



    public function editar(){
        Utilidades::isAdmin();

        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $producto = new Producto();
            $producto->setId($id);
            $product = $producto->fetchById();

            $edit = true;
            require_once 'views/producto/crear.php';

        }else{
            header("Location:" . base_url . 'producto/gestion');
        }

    }



    public function eliminar(){
        Utilidades::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $producto = new Producto();
            $producto->setId($id);
           $delete = $producto->delete();

           if($delete){
               $_SESSION['delete'] = 'completed';

           }else {
               $_SESSION['delete'] = 'failed';
           }

        }else {
            $_SESSION['delete'] = 'failed';
        }

        header("Location:" . base_url . 'producto/gestion');
    }





}
