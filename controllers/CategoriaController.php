<?php

require_once 'models/Categoria_empresa.php';
class CategoriaController{

    public function gestion(){
        Utilidades::isAdmin();
        $categorias_empresas = Utilidades::showCategoriasEmpresas();
        $categorias_productos = Utilidades::showCategoriasProductos();
        require_once 'views/categoria/gestion.php';
    }


    public function crear_categoria_empresa(){
    Utilidades::isAdmin();
    require_once 'views/categoria/empresa/crear.php';
}



    public function save_categoria_empresa(){
        Utilidades::isAdmin();

        if(isset($_POST['nombre'])){
            // Guardamos la categoria de empresa en la base de datos
            $categoria_empresa = new Categoria_empresa();
            $categoria_empresa->setNombre($_POST['nombre']);
            $categoria_empresa->save();
        }

        // redirigimos a la pagina de gestion de categorias
        header("Location:" . base_url . "categoria/gestion");

    }



    public function crear_categoria_producto(){
        Utilidades::isAdmin();
        require_once 'views/categoria/producto/crear.php';
    }



    public function save_categoria_producto(){
        Utilidades::isAdmin();

        if(isset($_POST['nombre'])){
            // Guardamos la categoria de empresa en la base de datos
            $categoria_producto = new Categoria_producto();
            $categoria_producto->setNombre($_POST['nombre']);
            $categoria_producto->save();
        }

        // redirigimos a la pagina de gestion de categorias
        header("Location:" . base_url . "categoria/gestion");
    }



    public function ver(){
        require_once 'models/Cesta.php';

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            // Sacamos la categoria
            $categoria_producto = new Categoria_producto();
            $categoria_producto->setId($id);
            $categoria = $categoria_producto->fetchById();


            // Sacamos las cestas que tengan productos de dicha categoria
            $cestas_categoria = new Cesta();
            $cestas = $cestas_categoria->getAllFromCategory($categoria->getId());
        }

        require_once 'views/categoria/ver.php';
    }





}