<?php
require_once 'models/Cliente.php';


class ClienteController{


    public function index(){
        echo "Controlador Cliente, Acción index";
    }



    public function registro(){
        require_once 'views/cliente/registro.php';
    }



    public function save(){
        if(isset($_POST)){
            $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
            $apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
            $poblacion = isset($_POST['poblacion']) ? $_POST['poblacion'] : false;
            $email = isset($_POST['email']) ? $_POST['email'] : false;
            $password = isset($_POST['password']) ? $_POST['password'] : false;

            if($nombre && $apellidos && $poblacion && $email && $password){
                $cliente = new Cliente();
                $cliente->setNombre($nombre);
                $cliente->setApellidos($apellidos);
                $cliente->setPoblacion($poblacion);
                $cliente->setEmail($email);
                $cliente->setPassword($password);

                // guarda en la base de datos los datos del usuario introducido en el formulario de views/cliente/registro.php
                $save = $cliente->save();

                if($save){
                    $_SESSION['register'] = 'completed';

                }else{
                    $_SESSION['register'] = 'failed';
                }

            }else{
                $_SESSION['register'] = 'failed';
            }

        }else{
            $_SESSION['register'] = 'failed';
        }

        header("Location:" . base_url . 'cliente/registro'); // redirige al formulario de registro pase lo que pase
    }



    public function login(){
        if(isset($_POST['email'], $_POST['password'])){

            // identificar al usuario
            $email = $_POST['email'];
            $password = $_POST['password'];

            // consulta a la base de datos
            $cliente = new Cliente();
            $client = $cliente->login($email, $password);

            // crear una sesion
            if($client && is_object($client)){
                $_SESSION['client'] = $client;

                if($client->rol == 'admin'){ // sesion especifica para administradores
                    $_SESSION['admin'] = true;

                }else{
                    $_SESSION['admin'] = false;
                }

                $_SESSION['error_login'] = null;

            }else {
                $_SESSION['error_login'] = 'Identificación fallida';

            }


        }
        header('Location:' . base_url);

    }



    public function logout(){
        if(isset($_SESSION['client'])){
            Utilidades::deleteSession('client');
        }

        if(isset($_SESSION['admin'])){
            Utilidades::deleteSession('admin');
        }

        header('Location:' . base_url);

    }


} // fin de la clase
