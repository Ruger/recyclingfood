<?php

require_once 'models/Cesta.php';
class CarritoController{

    public function index(){
        if(isset($_SESSION['carrito'])){
            $carrito = $_SESSION['carrito'];
        }

        require_once 'views/carrito/index.php';
    }



    public function add(){
        if(isset($_GET['id'])){
            $cesta_id = $_GET['id'];

        }else{
            header('Location:' . base_url);
        }

        // si ya existe el carrito
        if(isset($_SESSION['carrito'])){
            $counter = 0;
            foreach ($_SESSION['carrito'] as $indice => $elemento){
                if($elemento['cesta_id'] == $cesta_id){
                    $counter++;
                }
            }

            // solo se registra en el carrito la cesta si no esta ya en el carrito
            if($counter == 0){
                // Cargar la cesta seleccionada
                $cest = new Cesta();
                $cest->setId($cesta_id);
                $cesta = $cest->getById();
                $empresa = $cest->getEnterprise();

                // Añadir al carrito
                if(is_object($cesta)){
                    $_SESSION['carrito'][] = array(
                        "cesta_id" => $cesta->id,
                        "cesta" => $cesta,
                        "empresa" => $empresa
                    );
                }
            }

        }else{
            // Cargar la cesta seleccionada
            $cest = new Cesta();
            $cest->setId($cesta_id);
            $cesta = $cest->getById();
            $empresa = $cest->getEnterprise();

            // Añadir al carrito
            if(is_object($cesta)){
                $_SESSION['carrito'][] = array(
                    "cesta_id" => $cesta->id,
                    "cesta" => $cesta,
                    "empresa" => $empresa
                );
            }
        }
        header('Location:' . base_url . 'carrito/index');
    }



    public function delete(){
        if(isset($_GET['id']) && isset($_SESSION['carrito'])){
            $carrito = $_SESSION['carrito'];
            $id_cesta = $_GET['id'];

            foreach ($carrito as $indice => $elemento){
                $cesta = $elemento['cesta'];
                if($cesta->id == $id_cesta){
                    unset($_SESSION['carrito'][$indice]);
                }
            }

            if(empty($_SESSION['carrito'])){
                Utilidades::deleteSession('carrito');
            }
        }

        header('Location:' . base_url . 'carrito/index');
    }



    public function delete_all(){
        Utilidades::deleteSession('carrito');
        header('Location:' . base_url . 'carrito/index');
    }


}