<?php
require_once 'models/Cesta.php';
require_once 'models/Producto.php';
class CestaController{

    public function index(){
        $cesta = new Cesta();
        $cestas = $cesta->getFresh(6);

        /* Renderizar vista  de destacados */
        require_once 'views/cesta/destacadas.php';
    }


    public function gestion(){
        Utilidades::isAdmin();

        $cesta = new Cesta();
        $cestas = $cesta->getAll();
        $empresas = Utilidades::showEmpresasConProductos();

        require_once 'views/cesta/gestion.php';
    }



    public function ver(){
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $cesta = new Cesta();
            $cesta->setId($id);
            $cest = $cesta->fetchById();

            $empresa = $cesta->getEnterprise();

            $productos = Utilidades::showProductosDeEmpresa($empresa->id);
            $lineas = Utilidades::showLineasCesta($id);
        }

        require_once 'views/cesta/ver.php';
    }



    public function crear(){
        Utilidades::isAdmin();
        $productos = Utilidades::showProductosDeEmpresa($_GET['empresa']);
        require_once 'views/cesta/crear.php';
    }



    public function save(){
        Utilidades::isAdmin();
        if(isset($_POST['poblacion'])){
            $poblacion = isset($_POST['poblacion']) ? $_POST['poblacion'] : false;
            $direccion = isset($_POST['direccion']) ? $_POST['direccion'] : false;
            $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : null;


            if($poblacion && $direccion){
                $cesta = new Cesta();
                $cesta->setEstadoId(1); // en venta
                $cesta->setPoblacion($poblacion);
                $cesta->setDireccion($direccion);
                $cesta->setDescripcion($descripcion);


                // se toma la cantidad seleccionada de cada producto para calcular el precio total de la cesta
                $productos = Utilidades::showProductos();
                $precio = 0;

                while ($pro = $productos->fetch_object()){
                    if(isset($_POST[$pro->id]) && $_POST[$pro->id] > 0){
                        $precio += $_POST[$pro->id] * $pro->precio;
                    }
                }


                // obliga a seleccionar algun producto para poder crear una cesta
                if($precio > 0){
                    $cesta->setPrecio($precio);

                }else{
                    $_SESSION['cesta'] = 'failed';
                    header('Location:' . base_url . 'cesta/gestion');
                    exit();
                }


                // guardar la imagen
                if(isset($_FILES['imagen'])){
                    $file = $_FILES['imagen'];
                    $filename = $file['name'];
                    $mimetype = $file['type'];

                    if($mimetype === 'image/jpg' || $mimetype == 'image/jpeg' || $mimetype === 'image/png' || $mimetype === 'image/gif'){
                        if(!is_dir('uploads/images')){
                            if (!mkdir('uploads/images', 0777, true) && !is_dir('uploads/images')) {
                                throw new \RuntimeException(sprintf('Directory "%s" was not created', 'uploads/images'));
                            }
                        }

                        move_uploaded_file($file['tmp_name'], 'uploads/images/' . $filename);
                        $cesta->setImagen($filename);
                    }
                }

                require_once 'models/Linea_cesta.php';
                if(isset($_GET['id'])){
                    // editar la cesta
                    $id = $_GET['id'];
                    $cesta->setId($id);
                    $save = $cesta->edit();

                    $productos = Utilidades::showProductos();

                    // se editan las lineas de cesta
                    while ($pro = $productos->fetch_object()){
                        if(isset($_POST[$pro->id]) && $_POST[$pro->id] >= 0){
                            $linea = new Linea_cesta();
                            $linea->setCestaId($id);
                            $linea->setProductoId($pro->id);
                            $linea->setCantidad($_POST[$pro->id]);
                            $linea_original = $linea->fetchByArguments();

                            if($linea_original){
                                $linea->setId($linea_original->id);
                                $linea->edit();

                                $stock = ($pro->stock + $linea_original->cantidad) - $_POST[$pro->id];
                                $linea->modifyStock($stock);


                            }else{
                                $linea->save();

                                $ultima_linea = Utilidades::showUltimaLineaCesta();
                                $stock = $pro->stock - $linea->getCantidad();
                                $ultima_linea->modifyStock($stock);
                            }

                        }
                    }


                }else{
                    // crear la cesta
                    $save = $cesta->save();

                    $ultima_cesta = Utilidades::showUltimaCesta();
                    $productos = Utilidades::showProductos();
                    $errores = 0;

                    // se crean las lineas de cesta
                    while ($pro = $productos->fetch_object()){
                        if(isset($_POST[$pro->id]) && $_POST[$pro->id] > 0){
                           $linea = new Linea_cesta();
                           $linea->setCestaId($ultima_cesta->getId());
                           $linea->setProductoId($pro->id);
                           $linea->setCantidad($_POST[$pro->id]);

                           $error = $linea->save();

                           if(!$error){
                               $errores++;

                           }else{
                               $ultima_linea = Utilidades::showUltimaLineaCesta();

                               $stock = $pro->stock - $linea->getCantidad();
                               $ultima_linea->modifyStock($stock);
                           }

                        }
                    }

                    if($errores > 0){
                        $save = false;
                    }
                }


                if($save){
                    $_SESSION['cesta'] = 'completed';

                }else{
                    $_SESSION['cesta'] = 'failed';
                }

            }else{
                $_SESSION['cesta'] = 'failed';
            }

        }else{
            $_SESSION['cesta'] = 'failed';
        }

        header('Location:' . base_url . 'cesta/gestion');
    }





    public function editar(){
        Utilidades::isAdmin();

        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $cesta = new Cesta();
            $cesta->setId($id);
            $cest = $cesta->fetchById();

            $empresa = $cesta->getEnterprise();
            $edit = true;

            $productos = Utilidades::showProductosDeEmpresa($empresa->id);
            $lineas = Utilidades::showLineasCesta($id);
            require_once 'views/cesta/crear.php';

        }else{
            header('Location:' . base_url . 'cesta/gestion');
        }

    }




    public function eliminar(){
        Utilidades::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $lineas = Utilidades::showLineasCesta($id);

            // antes de borrar una cesta, se devuelve el stock a los productos y se borran sus respectivas lineas de cesta
            while ($lin = $lineas->fetch_object()){
                $linea = new Linea_cesta();
                $linea->setId($lin->id);
                $linea = $linea->fetchById();

                $producto = Utilidades::showProducto($linea->getProductoId());
                $stock = $producto->stock + $linea->getCantidad();
                $linea->modifyStock($stock);

                $linea->delete();
            }


            $cesta = new Cesta();
            $cesta->setId($id);
            $delete = $cesta->delete();

            if($delete){
                $_SESSION['delete'] = 'completed';

            }else {
                $_SESSION['delete'] = 'failed';
            }

        }else {
            $_SESSION['delete'] = 'failed';
        }

        header("Location:" . base_url . 'cesta/gestion');
    }



    public function pagar(){
        Utilidades::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $cesta = new Cesta();
            $cesta->setId($id);
            $cesta->setEstadoId(3);
            $pay = $cesta->pay();
        }

        header("Location:" . base_url . 'cesta/gestion');
    }



}
